[![build status](https://gitlab.com/researcher/alice-callback-middleware/badges/master/build.svg)](https://gitlab.com/researcher/alice-callback-middleware/commits/master)
[![coverage report](https://gitlab.com/researcher/alice-callback-middleware/badges/master/coverage.svg)](https://gitlab.com/researcher/alice-callback-middleware/commits/master)

#### ===== ALICE CALLBACK MIDDLEWARE ===== ####

Alice callback consumer middleware. Parses optional amqplib payload and add addional callback method into consumer context
