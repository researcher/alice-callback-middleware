/* global describe, before, beforeEach, after, afterEach, it, chai, assert */

const callback = require('./../../lib/consumermiddleware').default;

describe('Consumer Middleware', function testCase() {
  it('callback()', function assertion(done) {
    assert.isFunction(callback);

    const ctx = {};
    const next = function next() { return Promise.resolve(); };

    callback(ctx, next)
      .then(function successResult() {
        assert.property(ctx, 'callback');
        assert.isFunction(ctx.callback);

        done();
      })
      .catch(done);
  });

  it('ctx.callback()', function assertion(done) {
    const replyTo = { exchange: 'test', type: 'direct', routingKey: 'test' };

    const ctx = {
      message: {
        properties: {
          replyTo: JSON.stringify(replyTo),
        },
      },
      publish: function publish(reply) {
        const additionalReply = {
          content: 'test',
        };

        Object.assign(additionalReply, replyTo);

        assert.deepEqual(additionalReply, reply);

        done();
      },
    };

    const next = function next() { return Promise.resolve(); };
    callback(ctx, next)
      .then(function successResult() {
        return ctx.callback('test');
      })
      .catch(done);
  });
});
