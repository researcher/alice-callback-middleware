/* global describe, before, beforeEach, after, afterEach, it, chai, assert */

const waitForCallback = require('./../../lib/publisherextension').default;

describe('Publisher Extension', function testCase() {
  it('waitForCallback()', function assertion(done) {
    assert.isFunction(waitForCallback);

    const ctx = {};
    const next = function next() { return Promise.resolve(); };

    waitForCallback(ctx, next)
      .then(function successResult() {
        assert.property(ctx, 'waitForCallback');
        assert.isFunction(ctx.waitForCallback);

        done();
      })
      .catch(done);
  });

  it('ctx.waitForCallback()', function assertion(done) {
    const replyTo = { exchange: 'test', type: 'direct', routingKey: 'test' };

    const ctx = {
      meta: {
        options: {
          // eslint-disable-next-line
          replyTo: JSON.stringify(replyTo),
        },
      },
      consume: function consume(reply, fns) {
        const rep = reply;
        delete rep.queue;
        assert.deepEqual(rep, replyTo);
        assert.lengthOf(fns, 2);

        done();
      },
    };
    const next = function next() { return Promise.resolve(); };

    waitForCallback(ctx, next)
      .then(function successResult() {
        return ctx.waitForCallback(function fn() {
          return Promise.resolve();
        });
      })
      .catch(done);
  });
});
