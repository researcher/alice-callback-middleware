
const callback = async (ctx, next) => {
  // eslint-disable-next-line no-param-reassign
  ctx.callback = async (content, options) => {
    const replyTo = { content };
    Object.assign(replyTo, JSON.parse(ctx.message.properties.replyTo));

    await ctx.publish(replyTo, options);
  };

  await next();
};

export default callback;
