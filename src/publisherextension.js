
import _ from 'lodash';

const waitForCallback = async (ctx, next) => {
  // eslint-disable-next-line no-param-reassign
  ctx.waitForCallback = async (fns) => {
    const replyTo = JSON.parse(ctx.meta.options.replyTo);

    if (_.isEmpty(replyTo.queue)) {
      replyTo.queue = `reply.${replyTo.routingKey}`;
    }

    await ctx.consume(replyTo, [fns, async (consumeContext, consumeNext) => {
      consumeContext.channel.close();

      await consumeNext();
    }]);
  };

  return await next();
};

export default waitForCallback;
