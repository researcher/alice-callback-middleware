"use strict";

exports.__esModule = true;

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var callback = function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(ctx, next) {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            // eslint-disable-next-line no-param-reassign
            ctx.callback = function () {
              var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(content, options) {
                var replyTo;
                return _regenerator2.default.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        replyTo = { content: content };

                        Object.assign(replyTo, JSON.parse(ctx.message.properties.replyTo));

                        _context.next = 4;
                        return ctx.publish(replyTo, options);

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, undefined);
              }));

              return function (_x3, _x4) {
                return _ref2.apply(this, arguments);
              };
            }();

            _context2.next = 3;
            return next();

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  }));

  return function callback(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = callback;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jb25zdW1lcm1pZGRsZXdhcmUuanMiXSwibmFtZXMiOlsiY2FsbGJhY2siLCJjdHgiLCJuZXh0IiwiY29udGVudCIsIm9wdGlvbnMiLCJyZXBseVRvIiwiT2JqZWN0IiwiYXNzaWduIiwiSlNPTiIsInBhcnNlIiwibWVzc2FnZSIsInByb3BlcnRpZXMiLCJwdWJsaXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUNBLElBQU1BO0FBQUEsd0VBQVcsa0JBQU9DLEdBQVAsRUFBWUMsSUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ2Y7QUFDQUQsZ0JBQUlELFFBQUo7QUFBQSxxRkFBZSxpQkFBT0csT0FBUCxFQUFnQkMsT0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1BDLCtCQURPLEdBQ0csRUFBRUYsZ0JBQUYsRUFESDs7QUFFYkcsK0JBQU9DLE1BQVAsQ0FBY0YsT0FBZCxFQUF1QkcsS0FBS0MsS0FBTCxDQUFXUixJQUFJUyxPQUFKLENBQVlDLFVBQVosQ0FBdUJOLE9BQWxDLENBQXZCOztBQUZhO0FBQUEsK0JBSVBKLElBQUlXLE9BQUosQ0FBWVAsT0FBWixFQUFxQkQsT0FBckIsQ0FKTzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUFmOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUZlO0FBQUEsbUJBU1RGLE1BVFM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBWDs7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFOOztrQkFZZUYsUSIsImZpbGUiOiJjb25zdW1lcm1pZGRsZXdhcmUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmNvbnN0IGNhbGxiYWNrID0gYXN5bmMgKGN0eCwgbmV4dCkgPT4ge1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgY3R4LmNhbGxiYWNrID0gYXN5bmMgKGNvbnRlbnQsIG9wdGlvbnMpID0+IHtcbiAgICBjb25zdCByZXBseVRvID0geyBjb250ZW50IH07XG4gICAgT2JqZWN0LmFzc2lnbihyZXBseVRvLCBKU09OLnBhcnNlKGN0eC5tZXNzYWdlLnByb3BlcnRpZXMucmVwbHlUbykpO1xuXG4gICAgYXdhaXQgY3R4LnB1Ymxpc2gocmVwbHlUbywgb3B0aW9ucyk7XG4gIH07XG5cbiAgYXdhaXQgbmV4dCgpO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgY2FsbGJhY2s7XG4iXX0=