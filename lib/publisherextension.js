'use strict';

exports.__esModule = true;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var waitForCallback = function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(ctx, next) {
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            // eslint-disable-next-line no-param-reassign
            ctx.waitForCallback = function () {
              var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(fns) {
                var replyTo;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        replyTo = JSON.parse(ctx.meta.options.replyTo);


                        if (_lodash2.default.isEmpty(replyTo.queue)) {
                          replyTo.queue = 'reply.' + replyTo.routingKey;
                        }

                        _context2.next = 4;
                        return ctx.consume(replyTo, [fns, function () {
                          var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(consumeContext, consumeNext) {
                            return _regenerator2.default.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    consumeContext.channel.close();

                                    _context.next = 3;
                                    return consumeNext();

                                  case 3:
                                  case 'end':
                                    return _context.stop();
                                }
                              }
                            }, _callee, undefined);
                          }));

                          return function (_x4, _x5) {
                            return _ref3.apply(this, arguments);
                          };
                        }()]);

                      case 4:
                      case 'end':
                        return _context2.stop();
                    }
                  }
                }, _callee2, undefined);
              }));

              return function (_x3) {
                return _ref2.apply(this, arguments);
              };
            }();

            _context3.next = 3;
            return next();

          case 3:
            return _context3.abrupt('return', _context3.sent);

          case 4:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  }));

  return function waitForCallback(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = waitForCallback;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9wdWJsaXNoZXJleHRlbnNpb24uanMiXSwibmFtZXMiOlsid2FpdEZvckNhbGxiYWNrIiwiY3R4IiwibmV4dCIsImZucyIsInJlcGx5VG8iLCJKU09OIiwicGFyc2UiLCJtZXRhIiwib3B0aW9ucyIsImlzRW1wdHkiLCJxdWV1ZSIsInJvdXRpbmdLZXkiLCJjb25zdW1lIiwiY29uc3VtZUNvbnRleHQiLCJjb25zdW1lTmV4dCIsImNoYW5uZWwiLCJjbG9zZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQ0E7Ozs7OztBQUVBLElBQU1BO0FBQUEsd0VBQWtCLGtCQUFPQyxHQUFQLEVBQVlDLElBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUN0QjtBQUNBRCxnQkFBSUQsZUFBSjtBQUFBLHFGQUFzQixrQkFBT0csR0FBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDZEMsK0JBRGMsR0FDSkMsS0FBS0MsS0FBTCxDQUFXTCxJQUFJTSxJQUFKLENBQVNDLE9BQVQsQ0FBaUJKLE9BQTVCLENBREk7OztBQUdwQiw0QkFBSSxpQkFBRUssT0FBRixDQUFVTCxRQUFRTSxLQUFsQixDQUFKLEVBQThCO0FBQzVCTixrQ0FBUU0sS0FBUixjQUF5Qk4sUUFBUU8sVUFBakM7QUFDRDs7QUFMbUI7QUFBQSwrQkFPZFYsSUFBSVcsT0FBSixDQUFZUixPQUFaLEVBQXFCLENBQUNELEdBQUQ7QUFBQSxpR0FBTSxpQkFBT1UsY0FBUCxFQUF1QkMsV0FBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUMvQkQsbURBQWVFLE9BQWYsQ0FBdUJDLEtBQXZCOztBQUQrQjtBQUFBLDJDQUd6QkYsYUFIeUI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBQU47O0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBQXJCLENBUGM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFBdEI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBRnNCO0FBQUEsbUJBZ0JUWixNQWhCUzs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQWxCOztBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQU47O2tCQW1CZUYsZSIsImZpbGUiOiJwdWJsaXNoZXJleHRlbnNpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5cbmNvbnN0IHdhaXRGb3JDYWxsYmFjayA9IGFzeW5jIChjdHgsIG5leHQpID0+IHtcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gIGN0eC53YWl0Rm9yQ2FsbGJhY2sgPSBhc3luYyAoZm5zKSA9PiB7XG4gICAgY29uc3QgcmVwbHlUbyA9IEpTT04ucGFyc2UoY3R4Lm1ldGEub3B0aW9ucy5yZXBseVRvKTtcblxuICAgIGlmIChfLmlzRW1wdHkocmVwbHlUby5xdWV1ZSkpIHtcbiAgICAgIHJlcGx5VG8ucXVldWUgPSBgcmVwbHkuJHtyZXBseVRvLnJvdXRpbmdLZXl9YDtcbiAgICB9XG5cbiAgICBhd2FpdCBjdHguY29uc3VtZShyZXBseVRvLCBbZm5zLCBhc3luYyAoY29uc3VtZUNvbnRleHQsIGNvbnN1bWVOZXh0KSA9PiB7XG4gICAgICBjb25zdW1lQ29udGV4dC5jaGFubmVsLmNsb3NlKCk7XG5cbiAgICAgIGF3YWl0IGNvbnN1bWVOZXh0KCk7XG4gICAgfV0pO1xuICB9O1xuXG4gIHJldHVybiBhd2FpdCBuZXh0KCk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCB3YWl0Rm9yQ2FsbGJhY2s7XG4iXX0=