##### expected usage: #####

```
import { Client, Router } from 'alice';
import aliceCallback from 'alice-callback-middleware';
import Model from 'your-model';
import io from 'socket-io';

Client.use(aliceCallback);

const router = new Router('user');

router.direct({ exchange: 'exchange', queue: 'user', route: 'find' }, async (ctx, next) => {
  const { id } = ctx.content;

  const user = Model.find(id);

  ctx.callback(user);

  await next();
});

const options = {
  replyTo: {
    exchange: 'replyXE',
    queue: 'replyQueue',
    routingKey: 'replyRoute',
    room: 'myRoom'
  }
};

Client
  .publishDirect({ exchange: 'exchange', routingKey: 'user.find', content: { id: 'uid' } }, options)
  .waitForCallback(async (ctx, next) => {
    const { user } = ctx.content;
    const { room } = ctx.properties.replyTo;

    io.to(room).emit(user);
  });

```
